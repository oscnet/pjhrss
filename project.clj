(defproject pjhrss "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [org.clojure/data.xml "0.0.6"]
                 [lib-noir "0.3.5"]
                 [compojure "1.1.3"]
                 [clabango "0.5"] ; for templating library for clojure,used by sms
                 [hiccup "1.0.2"]
                 [ring/ring-jetty-adapter "1.1.0"]
                 [bultitude "0.1.7"]
                 [com.taoensso/timbre "1.2.0"]
                 [com.taoensso/tower "1.2.0"]
                 [markdown-clj "0.9.13"]
                 [org.clojure/java.jdbc "0.2.3"]
                 [clj-http "0.6.3"]
                 [ring.velocity "0.1.2"]
                 [org.clojure/google-closure-library-third-party "0.0-2029"] ;domina need this
                 [domina "1.0.0"]
                 [jayq "2.2.0"]
                 [org.apache.poi/poi "3.8-beta5"]
                 [org.apache.poi/poi-ooxml "3.8-beta5"] ;for access word excel file 
                 [clojurewerkz/quartzite "1.0.1"]
                 [net.sourceforge.jtds/jtds "1.2.4"]  ; for ms  sql server database
                 [org.xerial/sqlite-jdbc "3.7.2"]]
  :plugins [[lein-ring "0.8.2"]
            [lein-cljsbuild "0.2.10"]
            [codox "0.6.4"]]
  :ring {:handler pjhrss.handler/war-handler
         :init pjhrss.handler/init
  			 :destroy pjhrss.handler/destroy}
  :main pjhrss.server
  :profiles
  {:dev {:dependencies [[ring-mock "0.1.3"]
                        [ring/ring-devel "1.1.0"]]}}
  
  ;:hooks [leiningen.cljsbuild]
  :cljsbuild
  {:builds
   [{:source-path "src-cljs",
     :compiler
     {:pretty-print true,
      :output-to "resources/public/js/hxapp.js",
      ;:optimizations :advanced,
      :optimizations :whitespace,
      :externs ["resources/externs/jquery-1.8.js"]}}]}
  )