(ns pjhrss.routes.home
  (:use compojure.core 
        hiccup.element
        hiccup.core
        hiccup.form
        pjhrss.util
        pjhrss.models.system
        pjhrss.models.access
        pjhrss.models.html
        [ring.velocity.core :only [render]])  
  (:require [pjhrss.views.layout :as layout]
            [noir.response :as response]
            [noir.session :as session]
            [pjhrss.views.node :as node]))
 

(defn home-left []
  [:div 
   (left-box "公告公示"  (node/get-last-nodes 300 10))
   (left-box "业务指南"  "this is test")
   (left-box "专题专栏"  "this is test")]) 
 
(defn home-right []
  [:div
    (row  (node/term-box 242 10 5)
          (box "图片新闻" 4 ""))
    (row (box "阳光政务banner" 9 "")) 
    (row  (node/term-box 242 10 5)  
          (node/term-box 243 10 4))])

(defn home-page []
  (page "首页"     
      [:div.row
       [:div.span3  (home-left)]
       [:div.span9  (home-right)]]
      (page-header (list [:i.icon-tasks] "&nbsp开发进度") 
       "网站正在开发之中,目前进度")   
      [:div.progress.progress-striped.active [:div.bar {:style "width:60%"}]]))  
              
      (comment message-box "mybox" "提示" "此操作有危险,是否继续?" 
		  	(button "确认" "btn" 
                "alert('you click ok')"
                (hide-dialog "mybox"))
  			(button "放弃" "btn btn-primary" (hide-dialog "mybox"))
  			;(do-script (show-dialog "mybox"))
				(button "显示对话框"  "btn btn-primary" (show-dialog "mybox"))       
        [:div "this is the main area"])

(defn about-page []
  (layout/common "关于我们" 
   "this is the story of pjhrss... work in progress"))

(defn page-noaccess []
  (page "没有权限"
  	[:div.alert [:strong "警告！"] "您没有权限访问此网页."]))

(defroutes home-routes 
  (GET "/" [] (home-page))
  (GET "/noaccess" [] (page-noaccess))
  (GET "/about" [] (about-page)))