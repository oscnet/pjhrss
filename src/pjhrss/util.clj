(ns pjhrss.util
  (:use hiccup.core)
  (:require [noir.io :as io]
            [pjhrss.config :as config]
            [clojure.test :as test]
            [markdown.core :as md]))

(defn format-time
  "formats the time using SimpleDateFormat, the default format is
   \"dd MMM, yyyy\" and a custom one can be passed in as the second argument"
  ([time] (format-time time "yyyy-MM-dd"))
  ([time fmt]
    (.format (new java.text.SimpleDateFormat fmt) time)))

(defn md->html
  "reads a markdown file from public/md and returns an HTML string"
  [filename]
  (->> 
    (io/slurp-resource (str "md/" filename))      
    (md/md-to-html-string)))

(defn- def-var? [var]
  "test if a var is defined"
  (boolean (resolve var)))


(defn load-ns [ns]
  (try 
    (require (symbol ns))
    true
    (catch Exception e
      (println "loading " ns ":" e)
      false)))
  

(defn get-module-symbol
  "从module-ns中取变量 sym.    (get-module-symbol 'pjhrss.test updates)"
  [module-ns sym]
    (if (load-ns module-ns)
    	(let [sym (symbol (str module-ns "/" sym))]
      	(if (or (test/function? sym) (def-var? sym))
        	(eval sym) nil)) nil))


(defn modules-run
  [fn-name & ext]
	(doseq [module config/app-modules]
    (let [sym (get-module-symbol 
               (str (:ns module) (apply str (flatten ext)))
               fn-name )]
    	(if sym (sym)))))

(defn- modules-fns!
  "取所有模块的函数的数组 like {module-name module-function}"
  [fn-name & ext]
	(map #(hash-map :module %
                  :name (:name %)
                  :fn (get-module-symbol 
                       (str (:ns %) (apply str (flatten ext)))
                       fn-name )) config/app-modules))  
  

(defmacro modules-routes [& route]
	(into []
  	(concat       
			(filter (complement nil?)
       (map #(let [sym# 
             (symbol (str (:ns %) "/" (:name %) "-routes"))]
             (if (def-var? sym#) sym# nil))         
           config/app-modules) ) route )))

(defmacro require_modules []
	(cons 'do (map 
   #(list 'require (list 'quote
          (symbol (:ns %)))) config/app-modules))
)

(defn modules_perms []
  (sort-by #(str (:group %) "-" (:title %))
		(filter (complement nil?)
      (reduce #(concat %1 (get-module-symbol (:ns %2) (str (:name %2) "-perms") )) '()         
           config/app-modules))))

(defmacro restricted
  "checks if any of the rules defined in wrap-access-rules match the method,
   if no rules match then the response is a redirect to \"/\""
  [method url params body]
  `(~method ~url ~params
     (let [rules# (:access-rules noir.request/*request*)]       
       (if (or (nil? rules#) 
               (some (fn [~'rule] (~'rule '~method ~url ~params)) rules#)) 
         ~body 
         (noir.response/redirect "/noaccess")))))

(defn to-title [^String ls ^Integer maxlen]
  (let [s (h ls)]        
    (if (> (count s) maxlen) (str (.substring s 0 maxlen) "...") s)))
     