(ns pjhrss.views.layout
  (:use hiccup.form
        hiccup.core
        pjhrss.util
        pjhrss.models.system
        [hiccup.element :only [link-to]] 
        [ring.velocity.core :only [render]]
        [hiccup.page :only [html5 include-js include-css]])
  (:require [pjhrss.views.menu :as menu]
            [noir.session :as session]))

;not use now include js at end of content
(defn parse-head! [head]
  (condp = head
    		:jquery-form (include-js	"/js/jquery.form.js")
        :editor	(include-js	"/ckeditor/ckeditor.js")
        head))

(defn base
  [title & content]
		(list 
     [:head
      [:meta {:charset "utf-8"}]
     	[:title title]
     	(include-css "/css/bootstrap.min.css")
     	(include-css "/css/screen.css")
      (include-js	"/js/jquery-1.8.2.min.js")
     	(include-js 	"/js/bootstrap.min.js")
     	(include-js 	"/js/app.js")]
  	 [:body content]))
           
(defn common [title & content]
  (if (vector? title)
  	(html5 (base 
            (get-variable-with-cache "siteName")
             title content))
    (html5 (base title content))))
       

(defn manager [title & content]
  (html [:div#admin_page.admin_page 
     	[:span.name title]
			[:div.admin_body content ]]))

