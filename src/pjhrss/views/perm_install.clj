(ns pjhrss.views.perm-install
  (:use pjhrss.models.system)
   (:require [clojure.java.jdbc :as sql]
             [noir.util.crypt :as crypt]
            [pjhrss.util :as util]))

(defn create-role-permission-table []
  (sql/with-connection (db-pjhrss)
    (sql/do-commands "drop table if exists role_permission")                       
  	(sql/create-table
     :role_permission
     [:rid "integer not null"]
     [:permission "varchar(128)"])
    (sql/do-commands
     ;"ALTER TABLE role_permission ADD PRIMARY KEY role_permission(rid,permission)"
     "create index role_permission_rid on role_permission(rid,permission)")))

(defn install []
  (create-role-permission-table))
  
(def updates {
  0.1  #(print "update test to ver 0.1")
  })