(ns pjhrss.views.menu
  (:use compojure.core
   			hiccup.form
        pjhrss.models.system
        pjhrss.util
        [hiccup.element :only [link-to]])
  (:require [clojure.java.jdbc :as sql]))
    

    
(defn create-menu	[menu]
  (sql/with-connection (db-pjhrss)
  	(row-id (sql/insert-record :menu menu))))


(defn- read-chilren-menus [mid]
  (sql/with-connection (db-pjhrss)
  	(sql/with-query-results res
    	["select * from menu where parent=? order by weight desc" mid]
      (doall res))))
                          
(defn read-menus
  "read all menus which is chilren of mid"
  [mid]
  (reduce #(assoc 
             %1 
             %2 
             (read-chilren-menus (:mid %2)))
          {}
          (read-chilren-menus mid)))



(defn menu-add []
  )
(defn menu-edit []
  )
(defn menu-delete []
  )

(defn page-menu []
  "修改菜单，显示菜单，可以用鼠标移动调整位置 now support only add edit"
  
  )

(defn theme-menu [mid]
  [:div.navbar {:class (str "menu-" mid)} 
    (into [:ol.nav]
    	(for [{:keys [title uri]} (read-menus mid)]
       	[:li (link-to uri title)]))])                    


(defroutes menu-routes 
  (GET "/menu/add" [] (page-menu))
  (GET "/menu/edit" [] (page-menu))
  (POST "/menu/add" [] (menu-add))
  (POST "/menu/edit" [] (menu-edit))
  (POST "/menu/delete" [id] (menu-delete)))
