(ns pjhrss.views.soap
  (:use compojure.core hiccup.element hiccup.form)
  (:require [clojure.data.xml :as xml]
   					[pjhrss.views.layout :as layout]
            [noir.response :as response]
            [clj-http.client :as client]))


(defn- calc-password
  "计算消息验证码，规则为：手机号码末四位*3+2249"
  [tel]  
  (let [code (apply str (nthnext  tel  (- (count tel) 4)))]
    (str (+ (* (Integer. code) 3) 2249))))

        
(defn- xml-message [tel message]
  (xml/emit-str  
				  (xml/element :infos {}
   					(xml/element :info {}
     					(xml/element :msg_id {} (xml/cdata "-1"))
     					(xml/element :password {} (xml/cdata (calc-password tel)))
     					(xml/element :src_tele_num {} (xml/cdata "106573079595"))
      				(xml/element :dest_tele_num {} (xml/cdata tel))
     					(xml/element :msg {} (xml/cdata message))))))
  
(defn- xml-message! [tel message]
  (xml/emit-str (xml/sexp-as-element 
				  [:infos 
   					[:info
     					[:msg_id (xml/cdata "-1")]
     					[:password (xml/cdata (calc-password tel))]
     					[:src_tele_num (xml/cdata "106573079595")]
      				[:dest_tele_num (xml/cdata tel)]
     					[:msg (xml/cdata message)]]])))  

(defn page-send-message []
  (layout/common "发送短信" '()
  	[:h1 "send message"]
   		(form-to [:post "/soap"]
      	[:p "mobie number:" (text-field "tel")]
      	[:p "message:" (text-area {:rows 10 :cols 40} "message")]
      	(submit-button "send"))))


(defn- soap-post
  "模拟SOAP post soap xml 报文 to soap server"
  [url soapxml]
  (client/post url 
     {:hearders {"Content-Type" "text/xml; charset=utf-8"
               	"User-Agent" "HX-SOAP/1.1.0"
               	"SOAPAction"	""}
     :body soapxml}))

(defn- soap-sms
  "create soap xml for sms"
  [corporation message]
  (xml/emit-str (xml/sexp-as-element
        [:SOAP-ENV:Envelope {:xmlns:SOAP-ENV "http://schemas.xmlsoap.org/soap/envelope/" 
         										 :xmlns:ns1 "http://DefaultNamespace"}
					[:SOAP-ENV:Body 
           [:ns1:sendmsg 
            [:ns1:in0 corporation]
            [:ns1:in1 message]]]])))
            
(defn- sms-ret 
  "parse the status value from sms soap server,return nil for error"
  [soap-result]
  (try
  	(let [ body (xml/parse-str (:body soap-result))
         retxml (->  body :content first :content first :content first :content first)]
    	(-> (xml/parse-str retxml) :content first :content second :content first))
  (catch Exception e nil)))


(defn send-sms [tel message]
  (let [ret (soap-post "http://111.1.8.42/webservice/services/sendmsg"
             (soap-sms "jh079595" (xml-message tel message)))]
    (sms-ret ret)))

(defn result-message [ret-value]
  (get {"0" 	"提交成功"
      	"-1"	"企业帐号错误"
       	"-2" 	"验证码格式错误"
       	"-3"	"接入号即服务代码错误"
       	"-4"	"手机号码错误"
       	"-5" 	"消息为空"
       	"-6" 	"消息太长"
       	"-7"	"验证码不匹配"
       	"-8" 	"时间格式不正确"} ret-value))
     
(defn send-message [tel message]
  (let [ret (send-sms tel message)]
    (result-message ret)))
   
(defroutes soap-routes
  (GET "/soap" [] (page-send-message))
  (POST "/soap" [tel message] (send-message tel message)))
	