(ns pjhrss.views.perm
  (:use compojure.core hiccup.element hiccup.form
        pjhrss.util
        pjhrss.models.system)
  (:require [pjhrss.views.layout :as layout]
            [clojure.java.jdbc :as sql]))
         
(defn set-role-permission [rid,permission]
  (sql/with-connection (db-pjhrss)
  	(sql/insert-record :role_permission {:rid rid :permission permission})))

(defn delete-permission [rid,permission]
  (sql/with-connection (db-pjhrss)
 		(sql/delete-rows :role_permission ["rid=? and permission=?" rid permission])))

(defn user_permission? [uid permission]
  (if (= uid 1) true
   (sql/with-connection (db-pjhrss)
  	 (sql/with-query-results res
    	 ["select count(*) as count from user_roles a, role_permission b
       			where a.uid=? and a.rid=b.rid and b.permission=?" uid permission]
       (pos? (:count (first res)))))))


(def perm-perms 
  '({:perm "权限分配" :description "对用户组进行权限设置"}))

(defn page-set-perm []
  )

(defn set-perm []
  )

(defn page-noaccess []
  (layout/common "无权访问" 
  	[:div.alert
     [:strong "警告！"]
     "试图访问需要权限的网页"]))

(def perm-perms
   '(
  		{:path "/perms" :title "权限分配" :menu true :group "用户管理" :perm "权限分配" }))

(defroutes perm-routes
  (restricted GET "/perms" [] (page-set-perm))
  (restricted POST "/perms" [] (set-perm))
  (GET "/noaccess" [] (page-noaccess)))
  