(ns pjhrss.views.term
  (:use compojure.core
   			hiccup.form
        hiccup.core
        hiccup.util
        pjhrss.util
        pjhrss.models.system
        pjhrss.models.html
        hiccup.element)
  (:require [clojure.java.jdbc :as sql]
            [pjhrss.views.layout :as layout]
            [noir.validation :as vali]
            [noir.response :as response]))

(defn- term-sort [term]
  (if (string? (:weight term)) 0 (:weight term) ))

(defn db-create-term
  "creates a term"
  [{:keys [name parent weight] :as term}]
 	(sql/with-connection (db-pjhrss)
    (row-id (sql/insert-record :term term))))

(defn- list-term
  ([terms] (list-term terms 0 0))
  ([terms parent level]
  	(let [ts (filter #(= (:parent %) parent) terms)]
    	(if (empty? ts) ()
  			(map #(conj 
           (list-term terms (:tid %) (inc level))
           (assoc % :level level)) (sort-by term-sort ts))))))

(defn- list-term-t
  ([terms] (list-term-t terms 0))
  ([terms parent]
  	(let [ts (filter #(= (:parent %) parent) terms)]
    	(if (empty? ts) ()
  			(map #(hash-map 
           :term
            %
           :child
           (list-term-t terms (:tid %)))
           (sort-by term-sort ts))))))

(defn exist?
  ([name] (exist? name 0))
  ([name parent]
    (sql/with-connection (db-pjhrss)
      (sql/with-query-results res
        ["select count(*) k from term where name=? and parent=?" name parent]                        
        (if (> (:k (first res)) 0) true false)))))

(defn db-read-terms []
  (sql/with-connection (db-pjhrss)
  	(sql/with-query-results res
    	["select * from term"]
      (doall res))))

(defn list-term-flatten []
  (flatten (list-term (db-read-terms))))

(defn db-children-terms [tid]
  (sql/with-connection (db-pjhrss)
  	(sql/with-query-results res
    	["select * from term where parent=?" tid]
        (doall res))))

(defn- childrens* [tid]
  (let [terms (db-children-terms tid)]
    (if (empty? terms) nil
       (into (reduce #(conj %1 (:tid %2) ) [] terms) (map #(childrens* (:tid %)) terms)))))


(defn childrens [tid]
  (filter (complement nil?) (flatten (childrens* tid)))) 

(defn db-read-term [tid]
  (sql/with-connection (db-pjhrss)
  	(sql/with-query-results res
    	["select * from term WHERE tid=?" tid]
      (first res))))

(defn db-delete-term [tid]
	(sql/with-connection (db-pjhrss)
  	(sql/delete-rows :term ["tid=?" tid])))

(defn safe-delete-term
  "delete the term and it's chilrens"
  [tid]
  (doall
   (map #(db-delete-term %)
       (flatten (childrens tid))))
  (db-delete-term tid))

(defn db-update-term [tid name parent weight]
  (sql/with-connection (db-pjhrss)
  	(sql/update-values
     :term
     ["tid=?" tid]
     {:name name :parent parent :weight weight})))

(defn safe-update-term 
  "do not use update-term directly" 
  [tid name parent weight]
  (let [old (db-read-term tid)]
    (do
    	(if (not= (:parent old) (Integer. parent))
      	(sql/with-connection (db-pjhrss)
      		(sql/update-values
         		:term
         		["parent=?" tid]
         		{:parent (:parent old)})))
      (db-update-term tid name parent weight))))

(defn parent-terms
  "return terms vector from root to this term:tid"
  [tid]
  (let [term (db-read-term tid) parent (:parent term)]
    (if (or (nil? parent) (zero? parent))
      [term]
  		(conj (parent-terms parent) term))))


(defn get-node-term [nid]
  (sql/with-connection (db-pjhrss)
  	(sql/with-query-results res
    	["select tid from node_term where nid=?" nid]
      (doall res))))

(defn node-term-exist? [nid tid]
  (if (zero? (sql/with-connection (db-pjhrss)
             		(sql/with-query-results res
                  ["select count(*) as count from node-term where nid=? and tid=?" nid tid]
                  (:count (first res))))) 
    false true))

(defn add-node-term 
  "set node term id"
  [nid tid]
  (if (node-term-exist? nid tid) 0
 		(sql/with-connection (db-pjhrss)
    	(sql/insert-record :node_term {:nid nid :tid tid}))))

(defn set-node-term
  "set node term id,delete all other node term"
  [nid tid]
  (sql/with-connection (db-pjhrss)
  	(sql/delete-rows :node_term ["nid=?" nid])
    (sql/insert-record :node_term {:nid nid :tid tid}))) 
 
(defn first-node-term
  "get the first node term id"
  [nid]
  (:tid (first (get-node-term nid))))
 

(defn show-terms 
  "返回 tid 目录列表 "
  [tid]
  [:ul.term
  	(for [term (parent-terms tid)]
  		(link-to (url "/term/edit/" (:tid term)) 
         		(h (:name term))))])

(defn breakcrumb-term [tid]
  (let [pterms (parent-terms tid)
        terms (butlast pterms)
        last-term  (last pterms)]
	[:ul.breadcrumb  
   [:li (link-to "/" "首页") [:span.divider ">"]]
    (for [term terms]
      [:li (link-to (url "/term/" (:tid term)) (h (:name term))) 
       [:span.divider ">"]])
   [:li (h (:name last-term))]]))
      
      
(defn terms-to-select []
  (let [terms (list-term-flatten)]
    (map #(vector 
           (str (apply str (repeat (:level %) "－－")) (h (:name %)))
           (:tid %)) terms)))
  
(defn show-list-terms []
  (let [terms (db-read-terms)]    
  	[:ol.term-list 
     (for [term terms] 
       [:li 
       	(str (apply str (repeat (:level term) "－－")) (h (:name term)))])]))


(defn- ul-list-term [terms parent]
  (let [ts (filter #(= (:parent %) parent) terms)]
    (if-not (empty? ts)
      [:ul 
       (map #(vector 
                :li 
               (link-to (url "/term/edit/" (:tid %)) (h (:name %)))
               (ul-list-term terms (:tid %))
              )
            (sort-by term-sort ts))])))
  
(defn page-term-list
  "list all terms , and all command to add term,del term"
  []
  (html 
   (breakcrumb-manager {} "分类管理")
   (form "term/edit" "icon-tags" "分类管理"
    [:p "&nbsp"]
		(ul-list-term (db-read-terms) 0)     
    [:div.add_term (link-to {:class "btn"} "/terms/add" "添加分类")])))
                     
(defn term-select
  "get a term select, value is the selected term id" 
  [name value]
  (let [ tid (if (nil? value) nil (Integer. value))]
  	[:select {:name name} 
   		(select-options (conj (terms-to-select) ["根目录" 0]) tid)]))
  
(defn page-add-term	
  [& [tid name weight]]
  (html 
   (form "/terms/add" "icon-tag" "添加分类"
    [:p "&nbsp"]
    (form-field "parent" "建在哪个分类目录下" nil         
       (term-select "parent" tid))         
    (form-text-field "name" "名称" name)
    (form-text-field "weight" "顺序" weight)
    (form-submit-button "新建分类"))))
   
   
(defn term-valid? [parent name weight]
  (vali/rule (#(boolean (re-matches #"\d+" %)) parent)
             ["parent" "目录错误"])
  (vali/rule (vali/min-length? name 3)
             ["name" "分类名至少3位"])
  (vali/rule (#(boolean (re-matches #"\d*" %)) weight)
             ["weight" "顺序应该是数字"])
  (not (vali/errors? "parent" "name" "weight")))


(defn term-add [parent name weight]
  (if (term-valid? parent name weight)
    (do
      (db-create-term {:name name 
                       :parent parent 
                       :weight (if (empty? weight) "0" weight)})
  		(response/redirect "/terms"))
    (page-add-term parent name weight)))
  
(defn page-term-edit [tid]
  (let [term (db-read-term tid)]
    (html 
      (message-box "conform-delete" "提示" "此操作将删除所有子分类,有一定风险,是否继续?"
		  	(button "确认" "btn" 
                (hide-dialog "conform-delete")
                "$('[name=op]').val('删除')"
                "$('#term-edit-form').ajaxSubmit(options)")
  			(button "放弃" "btn btn-primary" 
                (hide-dialog "conform-delete")))
     
     (form {:id "term-edit-form"} (url "/term/edit/" tid) "icon-tag" "编辑分类"
     	[:p "&nbsp"]
      (hidden-field "op" "")
  		(form-field "parent" "上一级分类目录" nil
       	(term-select "parent" (:parent term)))
      (form-text-field "name" "分类名"  (:name term))
  		(form-text-field "weight" "顺序" (:weight term))
      (form-buttons
       (submit-button {:class "btn btn-primary"
                       :onclick "$('[name=op]').val('修改'); return true"
                       } "修改") 
       "&nbsp&nbsp&nbsp"
       (submit-button {:class "btn"
                       :onclick  (str (show-dialog "conform-delete") "; return false")            
                       } "删除"))))))
      
(comment defn updatable? [tid new-parent]
  (let [ term (db-read-term tid)]
  	(cond
			(empty? term) false
      (= tid new-parent) false
     	(zero? (Integer. new-parent)) true
     	((complement empty?) (db-read-term new-parent)) true
     :else false)))
      
(defn term-edit-valid? [tid parent name weight]
    (vali/rule ( (complement empty?) (db-read-term tid))
               ["name" "非法参数"])
    (vali/rule (not= tid parent)
               ["parent" "分类目录不能是自己"])
    (vali/rule ( #(or (= %1 "0") ((complement empty?) %2)) parent (db-read-term parent))
               ["parent" "分类目录错误"])
    (not (vali/errors? "parent" "name" "weight")))
          			    
(defn term-edit [op tid name new-parent weight]
  ;(println "call op=" op "tid=" tid)  
  (if (= op "删除")
    (do (safe-delete-term tid) (response/redirect "/terms"))
    (if (= op "修改")
      (if (term-edit-valid? tid new-parent name weight) 
        (do (safe-update-term tid name new-parent weight)
            (response/redirect "/terms"))
      	(page-term-edit tid)))))

(def term-perms
  [{:path "/terms" :title "分类列表" :group "分类管理" :perm "查看分类列表" :menu true}
   {:path "/terms/add" :title "增加分类" :group "分类管理" :perm "增加分类" :menu false}
   {:path "/terms/edit/:tid" :title "编辑分类" :group "分类管理" :perm "编辑分类" :menu false}])
   
                    
(defroutes term-routes
  (GET "/terms"			[] (page-term-list))
  (restricted GET "/terms/add" [] (page-add-term))
  (restricted POST "/terms/add" [parent name weight] (term-add parent name weight))
  (restricted GET "/term/edit/:tid" [tid] (page-term-edit tid))
  (restricted POST "/term/edit/:tid" [op tid name parent weight] (term-edit op tid name parent weight)))