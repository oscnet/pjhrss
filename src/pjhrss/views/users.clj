(ns pjhrss.views.users
  (:use compojure.core 
        hiccup.element 
        hiccup.form
        hiccup.core
        hiccup.util
        pjhrss.util
        pjhrss.models.system
        pjhrss.models.access
        pjhrss.models.html
        [ring.velocity.core :only [render]]
        [hiccup.page :only [include-js include-css]])
  (:require [pjhrss.views.layout :as layout]
            [noir.session :as session]
            [noir.validation :as vali]
            [noir.util.crypt :as crypt]
            [clojure.java.jdbc :as sql]
            [pjhrss.views.menu :as menu]
            [noir.response :as response]))

(defn get-user [name]
  (sql/with-connection (db-pjhrss)
    (sql/with-query-results 
      res ["select * from user where name = ?" name] (first res))))

(defn read-user [uid]
  (sql/with-connection (db-pjhrss)
    (sql/with-query-results res 
      ["select * from user where uid = ?" uid] (first res))))

(defn read-users []
  (sql/with-connection (db-pjhrss)
  	(sql/with-query-results res
    	["select * from user ORDER BY name ASC"]
      (doall res))))

(defn create-user
  "creates a user row with name and pass columns"
  [{:keys [name pass] :as user}]
  (if (get-user name) 
    false
  	(sql/with-connection (db-pjhrss)
    	(row-id (sql/insert-record :user user)))))

(defn delete-user [uid]
  (sql/with-connection (db-pjhrss)
  	(sql/delete-rows :user
      ["uid=?" uid])))

(defn create-role [role]
  (sql/with-connection (db-pjhrss)
  	(row-id (sql/insert-record :role role))))

(defn delete-role [rid]
  (sql/with-connection (db-pjhrss)
  	(sql/delete-rows :role_permission ["rid=?" rid])
    (sql/delete-rows :user_roles ["rid=?" rid])
    (sql/delete-rows :role ["rid=?" rid])))

;add primary key (uid,rid) for not repeat
(defn add-user-roles [rid,uid]
  (sql/with-connection (db-pjhrss)
  	(sql/insert-record :user_roles {:uid uid :rid rid})))

(defn delete-user-roles [rid,uid]
  (sql/with-connection (db-pjhrss)
  	(sql/delete-rows :user_roles ["rid=? and uid=?" rid uid])))

;-----------------------------------------------------------------------------------

(defn- page-user-template [title post & main]
   (page title 
     (breakcrumb '() title)
     (form post "icon-user" title main)))
  
(defn page-show-users []
  (html 
   (breakcrumb-manager {} "用户列表")
   (form "#" "icon-user" "用户列表"
    [:p "&nbsp"]
    [:div.row
     [:div.span8.offset2
    	[:table.table.table-striped.table-bordered.table-hover
     		[:thead
        	(into [:tr]
        		(for [title '("用户名" "邮箱" "手机" "上次登陆时间" "有效")]
            	[:th title]))]
        	(for [{:keys [name email mobie last_login is_active]} (read-users)]
          	[:tr
           		[:td (h name)]
           		[:td (h email)]
           		[:td (h mobie)]
           		[:td (h last_login)]
           		[:td (h is_active)]])]]])))            
                     
(defn page-register-user [& [name]]
  (page-user-template  
   "注册用户" 
   "/user/register"
      (form-text-field "name" "帐号名称" name "输入帐号名称" "张三")
    	(form-password-field "pass" "设定密码" nil "输入密码")
      (form-password-field "pass2" "重输密码" nil "重输密码")
   		(form-submit-button "用户注册")
   		(do-script "$('#name').focus()")))

             
(defn- user-valid? [name pass pass2]
  (vali/rule (vali/min-length? name 3)
             ["name" "用户名至少3位"])
  (vali/rule (vali/max-length? name 10)
             ["name" "用户名至多10位"])
  (vali/rule (vali/min-length? pass 5)
             ["pass" "密码至少5位"])
  (vali/rule (#(= %1 %2) pass pass2)
             ["pass2" "两次输入的密码不一样"])
  (vali/rule (not (empty? name))
             ["name" "用户名没有填写"])
  (vali/rule (#(or (empty? %) (not (get-user %))) name)
             ["name" (str "用户 " (h name) " 已经存在")])
  (not (vali/errors? "name" "pass" "pass2")))

(defn register-user [name pass pass2]
  (if (user-valid? name pass pass2)
    (do 
 			(create-user {:name name :pass (crypt/encrypt pass)})
    	(page-user-template	"注册用户" "#"
      	[:div.hero-unit
         [:h1 (str "你好," (h name))]
         [:p "感谢您注册我们的网站,如果对网站有好的建议和想法,请跟我们联系"]
         [:p [:a.btn.btn-primary.btn-large {:href "/user/login"} "点此登陆"]]])) 
       (page-register-user name)))

(defn page-login [ & [name error]]
  (page-user-template 
   "用户登录"
   "/user/login"
   [:p "&nbsp"]
   (form-text-field "name" "用户名" name "输入帐号名称")
   (form-password-field "pass" "输入密码" nil "输入密码")
   (form-submit-button "登陆")
   (do-script "$('#name').focus()")))
  
(defn page-user-info [uid]
  (let [{:keys [name email mobie admin last_login is_active]} (read-user uid)]
   (page-user-template "用户信息" "#"
  	 [:p "&nbsp"]
     (form-text-field "name" "用户名" (h name))
     (form-text-field "email" "邮箱"  (h email))
     (form-text-field "mobie" "手机" (h mobie))
     (form-text-field "admin" "系统用户?" (h admin))
     (form-text-field "last_login" "上次登陆" (h last_login))
     (form-text-field "is_active" "有效" (h is_active)))))                  
 
(defn page-user-info-show-in-manager [uid]
  (let [{:keys [name email mobie admin last_login is_active]} (read-user uid)]
    (html
      (breakcrumb-manager '() "用户信息")
     	(form "#" "icon-user" "用户信息"
  	 	[:p "&nbsp"]
     	(form-text-field "name" "用户名" (h name))
     	(form-text-field "email" "邮箱"  (h email))
     	(form-text-field "mobie" "手机" (h mobie))
     	(form-text-field "admin" "系统用户?" (h admin))
     	(form-text-field "last_login" "上次登陆" (h last_login))
     	(form-text-field "is_active" "有效" (h is_active))))))

(defn page-roles-show-in-manager [& [op roleNmae roles]]
	(condp = op
		"删除"
    	(do)
        
    "增加"
			(do)
    :else
    	))

(defn login [name pass]
  (let [user (get-user name)]
  	(if (and user (crypt/compare pass (:pass user)))
    	(do 
        (session/put! :user user)
        (response/redirect "/"))
      (do 
        (vali/rule false ["name" "登陆失败,用户名或密码错误!"])
      	(page-login name)))))

(defn logout []
	(session/clear!)
  (response/redirect "/")
  )




(def users-perms 
  [{:path "/users" :title "用户列表" :group "用户管理" :perm "查看用户列表" :menu true}
   {:path "/user/:uid" :title "用户信息" :group "用户管理" :perm "查看用户信息" :menu false}])

(defroutes users-routes
  (restricted GET "/users" [] (page-show-users))
  (GET "/user/register" [] (page-register-user ))
  (POST "/user/register" [name pass pass2] (register-user name pass pass2))
	(GET	"/user/login" [name error] (page-login name error))  
  (POST "/user/login" [name pass] (login name pass))
  (POST "/user/logout" [] (logout))
	(GET  "/user/logout" [] (logout))
  (restricted GET "/user/roles" [] (page-roles-show-in-manager))
  (restricted POST "/user/roles" [op role-name roles] (page-roles-show-in-manager op role-name roles))
  (restricted GET "/userInfo/:uid" [uid] (page-user-info-show-in-manager uid))
  (restricted GET	"/user/:uid"	[uid] (page-user-info uid)))
