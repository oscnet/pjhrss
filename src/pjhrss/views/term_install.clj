(ns pjhrss.views.term-install
  (:use pjhrss.models.system)
   (:require [clojure.java.jdbc :as sql]
            [pjhrss.util :as util]))

(defn- create-term-table
  "create term table"
  []
  (sql/with-connection (db-pjhrss)
  	(sql/create-table
     :term
     [:tid "integer PRIMARY KEY AUTOINCREMENT"]
     [:parent "integer default 0"]
     [:name	"varchar(64)"]
     [:weight "integer default 0"])
  	(sql/do-commands
     "create index term_parent on term(parent)")))

(defn- create-node-term-table
  "create node term table"
  []
  (sql/with-connection (db-pjhrss)
  	(sql/create-table 
     :node_term
     [:tid	"integer"]
     [:nid	"integer"])
    (sql/do-commands
     "create index node_term_tid on node_term(tid)"
     "create index node_term_nid on node_term(nid)")))

(defn install []
  (create-term-table)
  (create-node-term-table))

(def updates {
  0.1  #(print "update test to ver 0.1")
  })