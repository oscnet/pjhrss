(ns pjhrss.views.node-install
   (:use pjhrss.models.system)
   (:require [clojure.java.jdbc :as sql]))

(defn- create-node-table
  "create node table"
  []
  (sql/with-connection (db-pjhrss)
  	(sql/create-table
     :node
     [:nid "integer PRIMARY KEY AUTOINCREMENT"]
     [:title "varchar(255)"]
     [:uid "integer"]
     [:body	"text"]
     [:status "integer"]
     [:modtime	"timestamp default current_timestamp"])
    (sql/do-commands
     "create index node_uid on node(uid)"
     "create index node_status on node(status)")))

(defn install []
  (create-node-table))

(def updates {
  0.1  #(print "update test to ver 0.1")
  })