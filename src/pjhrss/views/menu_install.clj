(ns pjhrss.views.menu-install
  (:use pjhrss.models.system)
  (:require [clojure.java.jdbc :as sql]
            [pjhrss.util :as util]))

(defn- create-menu-table
  "create menu table"
  []
  (sql/with-connection (db-pjhrss)
   	(sql/create-table
     :menu
     [:mid "integer PRIMARY KEY AUTOINCREMENT"]
     [:parent "integer default 0"]
     [:title "varchar(128)"]
     [:uri	"varchar(255)"]
     [:weight	"integer"]
     [:permission "varchar(128)"]
     [:description "text"])
    (sql/do-commands
     "create index menu_parent on menu(parent)")))

(defn install []
  (create-menu-table)
  (create-object :menu {:mid 1 :parent 0 :title "主菜单" 		:weight 0})
  (create-object :menu {:mid 2 :parent 0 :title "副菜单" 		:weight 1})
  (create-object :menu {:mid 3 :parent 0 :title "用户菜单" 	:weight 2}))
  
(def updates {
  0.1  #(print "update test to ver 0.1")
  })