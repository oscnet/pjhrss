(ns pjhrss.views.node
  (:use compojure.core 
        hiccup.element 
        hiccup.form
        hiccup.core
        hiccup.util
        pjhrss.util
        pjhrss.models.system
        pjhrss.models.html
        pjhrss.models.access
        [clojure.string :only [split]]
        [ring.velocity.core :only [render]])  
  (:require [pjhrss.views.layout :as layout]
            [noir.session :as session]
            [noir.validation :as vali]
            [clojure.java.jdbc :as sql]
            [pjhrss.views.term :as term]
            [noir.response :as response]
            [pjhrss.views.users :as users]
            [pjhrss.models.pager :as pager]
            [pjhrss.config :as config]
            [pjhrss.models.file :as file]))
 

 (defn db-read-nodes []
  (sql/with-connection (db-pjhrss)
  	(sql/with-query-results res
    	["select * from node ORDER BY modtime DESC"]
      (doall res))))
 
(defn read-nodes-page [tid page]
  (let [page (if (nil? page) 0 page)
        tids (conj (term/childrens tid) tid)
        stids (clojure.string/join "," tids)]
  	(if (or (nil? tid) (zero? tid))
  		(pager/query-page page 
              config/manager-nodes-per-page
                "select * from node ORDER BY modtime DESC")
    	(pager/query-page page 
              config/manager-nodes-per-page
             	(str "select * from node a, node_term b 
               		 where a.nid = b.nid and b.tid in (" stids ") ORDER BY a.modtime DESC"
              )))))

(defn total-records [& [tid]]
  (if (zero? tid)
      (pager/records "select count(*) from node ORDER BY modtime DESC")
      (pager/records "select count(*) from node a, node_term b 
               	 where a.nid = b.nid and b.tid =? ORDER BY a.modtime DESC"
           tid)))

(defn db-read-node [nid]
  (sql/with-connection (db-pjhrss)
  	(sql/with-query-results res
    	["select * from node WHERE nid=?" nid]
      (first res))))

(defn- db-create-node* 
  "create a node "
  [node]
  (sql/with-connection (db-pjhrss)
    (row-id (sql/insert-record :node node))))


(defn db-create-node 
  "create a node "
  [node]
  (db-create-node* 
   (if (contains? node :modtime) 
     node 
     (assoc node :modtime (new java.util.Date)))))
  
        
(defn db-update-node [nid title body]
  (sql/with-connection (db-pjhrss)
  	(sql/update-values
     :node
     ["nid=?" nid]
     {:title title :body body :modtime (new java.util.Date)})))


(defn db-delete-node [nid]
  (sql/with-connection (db-pjhrss)
  	(sql/delete-rows :node
    	["nid =?" nid])))

(defn- set-node-files
  "set fids to the node id, fids is string like 16,32"
  [nid fids]
  (file/set-node-files nid (split fids #",")))


(defn show-nodes	[& [tid page]]
  (let [tid  (if (nil? tid) 0 (Integer. tid))
        page (if (nil? page) 1 (Integer. page))]
  [:div 
   [:table.table.table-striped
     (for [{:keys [nid title body modtime uid]} (read-nodes-page tid page)]
         [:tr
          [:td (link-to (url "/edit/node/" nid) (h title))]
          (let [user (users/read-user uid)]
          	[:td.author (link-to (url "/userInfo/" uid) (h (:name user)))])
         	[:td (term/show-terms (term/first-node-term nid))]  
          [:td (format-time modtime)]])]
   (pager/pager page (pager/pages (total-records tid) config/manager-nodes-per-page)            
    	"/nodes" (hash-map :tid tid)
    )]))

(defn page-show-nodes [& [tid page]]
  ;(println "tid=" tid "page=" page)
  (html
   (breakcrumb-manager '() "文章列表")
   (form-to {:class "form-inline"} [:get "/nodes"]
   	(term/term-select "tid" tid)
    [:button.btn {:type "submit"} "确定"])  
   (show-nodes tid page)))

(defn- node-submit-js[]
   "var editor_data = CKEDITOR.instances.body.getData();
    $('#body').val(editor_data);
    $('#fids').val(fileFids);
    return true;")
    
(defn page-add-node [& [tid title body]]
  (html
  	(breakcrumb-manager '() "新增文章")
   	(show-form-message)
   	[:div.node-edit
  		(form
        {:id "node-form"}
       	"/add/node" "icon-list-alt" "新增文章"
        (hidden-field "fids")
        (form-field "tid" "文章分类" nil
          (term/term-select "tid" tid))
 				(form-text-field "title" "文章标题" title)
       
        (form-field "body" "正文" "文章内容"
        		(text-area {:rows 50 :cols 40} "body" body))
        (do-script "CKEDITOR.replace('body')")
        (form-buttons
        	(submit-button {:name "op" :onclick (node-submit-js)} "保存")))]
   	(file/file-upload-form)))

  
(defn- page-edit-node* [nid title body tid]
  	(html 
   		(breakcrumb-manager '() "编辑文章")  
      (show-form-message)
      [:div.node-edit
       (form 
        {:id "node-form"}
        (str "/edit/node/" nid) "icon-list-alt" "编辑文章"
        (form-field "tid" "文章分类" nil
          (term/term-select "tid" tid))
        (form-text-field "title" "文章标题" title)       
        (form-field "body" "正文" "文章内容"
        		(text-area {:rows 50 :cols 40} "body" body))
        (do-script "CKEDITOR.replace('body')")
        (hidden-field "fids")
        (form-buttons
        	(submit-button {:name "op" :onclick (node-submit-js)} "确定修改")
         	"&nbsp&nbsp"
          (submit-button {:name "op"} "删除")))
       (file/file-upload-form nid)]))
  
(defn page-edit-node [nid & [title body tid]]
  (if (nil? title) 
  	(let [{:keys [nid title body]} (db-read-node nid)
           tid (term/first-node-term nid)] 
      (page-edit-node* nid title body tid))
    (page-edit-node* nid title body tid)))
    
(defn node-valid? [tid title body ]
  (vali/rule ((complement empty?) title)
             ["title" "文章不能没有标题"])
  (vali/rule (vali/min-length? body 15)
             ["body" "文章内容不能少于15个字"])
  (not (vali/errors? "title" "body")))
    
(defn update-node [op nid title body tid fids]
  ;(println body)
  (condp = op
  	"删除"  
        (do 
          (file/delete-node-files nid)
          (db-delete-node nid)
          (response/redirect "/nodes"))
    "确定修改"
    		(do 
          (set-node-files nid fids)
        	(if (node-valid? tid title body)
  					(let [[affected] (db-update-node nid title body)]
  						(if (zero? affected)
      					(do
        					(set-form-message "发生错误，不能保存数据!")
        					(page-edit-node nid title body tid))
      					(do (term/set-node-term nid tid) (response/redirect "/nodes"))))
    				(do
        			(set-form-message "发生错误，不能保存数据!")
        			(page-edit-node nid title body tid))))
   :else	(response/redirect "/noaccess")))



(defn page-show-node [nid]
  (let [{:keys [nid title body modtime uid]} (db-read-node nid)
          {:keys [name]} (users/read-user uid)
        tid (term/first-node-term nid)
        pterms (term/parent-terms tid)
        bc (map #(hash-map :path (str "/term/" (:tid %))
                   :title (h (:name %))) pterms)]
    ;(println pterms bc)
    (page (h title)
      (breakcrumb bc (h title))
      [:div#node-page 
       [:h3 (h title)]
       [:div.info [:span.date (format-time modtime)] [:span.author " 来源：" (h name)]]
       [:div.body body]
       (file/node-files-show nid)])))

			  
(defn- add-files-to-node [fids nid]
  (let [fids (split fids #",")]
    (doseq [fid fids]
      (file/set-node-file nid fid))))   

(defn save-node [tid title body file fids]
  (println "fids" fids (class fids)) ;String like "62,63" 
  (if (node-valid? tid title body)    
  	(let [nid (db-create-node {
                            :title title 
                            :body  body  
                            :uid (:uid (session/get :user))})]    
    	(if (> nid 0) 
      	(do 
        	(term/set-node-term nid tid)
          (if-not (empty? fids) (add-files-to-node fids nid))
        	;(file/file-upload-submit nid file)
          (set-node-files nid fids)
        	(set-form-message "文章已经保存")
        	(page-add-node))
      	(do
        	(set-form-message "警告,发送的文章没有被保存")
      		(page-add-node tid title body))))
    (page-add-node tid title body)))
      
(def node-perms
  [
    {:path "/edit/node/:nid" :perm "编辑文章" :title "编辑文章" :group "文章管理" :menu false :description "编辑文章"}
    {:path "/nodes" :perm "文章列表" :title "文章列表" :group "文章管理" :menu true :description "文章列表"}
    {:path "/add/node" :perm "新建文章" :title "新建文章" :group "文章管理" :menu true :description "新建文章"}])


(defn get-last-nodes [tid nodes]
  (let [tids (conj (term/childrens tid) tid)
        sql  (str "select a.nid,a.title,a.modtime from node a, node_term b 
                     where a.nid = b.nid and b.tid in (" 
                  (clojure.string/join "," tids)
                  ") ORDER BY a.modtime DESC")
        nodes (pager/query-page
               1 
               nodes
               sql)]
    [:ul
    (for [node nodes]
      [:li (link-to {:alt (h (:title node))} 
                    (str "/node/" (:nid node))
                    (to-title (:title node) 15))
       [:div.box-modtime (format-time (:modtime node))]])]))
    
(defn term-box
  "show box last newest nodes "
  [tid nodes width]
  (let [title (h (:name (term/db-read-term tid)))
        content (get-last-nodes tid nodes)]
    (box title width content)))

(defn- list-nodes	[& [tid page]]
 (let [tid  (if (nil? tid) 0 (Integer. tid))
        page (if (nil? page) 1 (Integer. page))]
  [:ul.list-nodes 
     (for [{:keys [nid title body modtime uid]} (read-nodes-page tid page)]
       ;(let [user (users/read-user uid)]
          [:li (link-to (url "/node/" nid) (h title))
           ;[:span.author (link-to (url "/userInfo/" uid) (h (:name user)))]  
           [:span.modtime (format-time modtime)]])         
     (pager/pager page (pager/pages (total-records tid) config/manager-nodes-per-page)            
    	(str "/term/" tid))]))

(defn page-show-term-nodes [tid page]
  (let [{:keys [name]} (term/db-read-term tid)] 
    (layout/common (h name)
                   (my-render "index.vm" 
        :user (:name (session/get :user))
        :manager (path_permission? "/manager")  
        :main (html (term/breakcrumb-term tid)
                    (list-nodes tid page))))))

(defroutes node-routes 
  (GET "/node/:nid" [nid] (page-show-node nid))
  (GET "/term/:tid" [tid page] (page-show-term-nodes tid page))  
  (restricted GET "/nodes" [tid page] (page-show-nodes tid page))
  (restricted GET "/add/node" [] (page-add-node))
  (GET "/node/:nid" [nid] (page-show-node nid))
  (restricted GET "/edit/node/:nid" [nid] (page-edit-node nid))
  (restricted POST "/edit/node/:nid" [op nid title body tid fids] (update-node op nid title body tid fids)) 
  (restricted POST "/add/node" [tid title body file fids] (save-node tid title body file fids)))
