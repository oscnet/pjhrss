(ns pjhrss.views.manager
  (:use 
   			compojure.core 
        hiccup.element hiccup.form
        pjhrss.util
        pjhrss.models.system
        hiccup.element
        hiccup.core
   			pjhrss.models.access
        pjhrss.models.html
        [ring.velocity.core :only [render]]
        [hiccup.page :only [include-js include-css]])
  (:require [pjhrss.views.layout :as layout]
            [noir.session :as session]
            [noir.util.crypt :as crypt]
            [clojure.java.jdbc :as sql]
            [pjhrss.views.menu :as menu]
            [noir.response :as response]))


(comment defn user-menu [mid active-mid]
	(for [[ k v ] (menu/read-menus 3)]
   (list 
    [:h3 (:title k)]
		[:div  [:ol 	
    	(for [m v]
      	[:li (if (= (:mid m) active-mid) {:class "active"}) 
         (link-to (:uri m) (:title m))])]])))

(defn- user-menu-groups []
  (set (reduce #(conj %1 (:group %2)) [] (filter #(:menu %) (modules_perms)))))

(defn- get-menus [group]
  (filter #(and (:menu %) (= group (:group %))) (modules_perms)))

(defn- user-menu []
  [:ul.nav.nav-list
  	(for [group (user-menu-groups)]
   		(if-not (empty? group)
    		(list [:li.nav-header group] 
    			(for [item (get-menus group)]
        		[:li (link-to (:path item) (:title item))]))))])
        		
(defn page-manager
  "用户管理界面"
  [active_mid & content]
  (page "管理界面"
        [:div.container-main
         [:div.row-fluid
          [:div.span2 
           [:div#user-menu.well	(user-menu)]]
          [:div.main_area.span10.main
           [:div#main_area content ]
   				 [:div.clear]
           (javascript-tag "
           		$(function() {        		
  							$('form').ajaxForm(options);
  							$('#user-menu a').click(setClick);
							})") 
   				 (include-js	"/js/jquery.form.js")
    			 (include-js	"/ckeditor/ckeditor.js")]]]))

(defn page-manager-main []
  "")
           
(def manager-perms 
  (list
  	{:path "/manager" :title "manager" :menu false :group "" :perm "网站管理" :description "可否进入网站管理界面"}))

(defroutes manager-routes
  (restricted GET "/manager"  []  (page-manager 0 "this is test page"))
  (restricted GET "/mindex"  []  (page-manager-main)))