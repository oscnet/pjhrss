(ns pjhrss.views.users-install
  (:use pjhrss.models.system)
   (:require [clojure.java.jdbc :as sql]
             [noir.util.crypt :as crypt]
            [pjhrss.util :as util]))

(defn create-user-table
  "creates the users table, the user has following fields
   id - "
  []
  (sql/with-connection (db-pjhrss)
    (sql/create-table
      :user
      [:uid "integer PRIMARY KEY AUTOINCREMENT"]
      [:name "varchar(64)"]
      [:email "varchar(30)"]
     	[:mobie "varchar(16)"]
      [:last_login :time]
      [:is_active :boolean]
      [:pass "varchar(100)"])
    (sql/do-commands
     "create index user_name on user(name)")))
(defn create-role-table []
  (sql/with-connection (db-pjhrss)
    (sql/do-commands "drop table if exists role")                   
  	(sql/create-table 
     :role
     [:rid "integer PRIMARY KEY AUTOINCREMENT"]
     [:name "varchar(128)"]
     [:weight "integer default 0"])
    (sql/do-commands
     "create index role_name_weight on role(name,weight)"
     "create UNIQUE index role_name on role(name)")))

(defn create-user-roles-table []
  (sql/with-connection (db-pjhrss)
    (sql/do-commands "drop table if exists user_roles")    
  	(sql/create-table
     :user_roles
     [:uid "integer not null"]
     [:rid "integer not null"])
    (sql/do-commands
     "create index user_role_rid on user_roles(rid)"
     ;"ALTER TABLE user_roles ADD PRIMARY KEY user_roles(uid,rid)"
     )))

(defn create-role-permission-table []
  (sql/with-connection (db-pjhrss)
    (sql/do-commands "drop table if exists role_permission")                       
  	(sql/create-table
     :role_permission
     [:rid "integer not null"]
     [:permission "varchar(128)"])
    (sql/do-commands
     ;"ALTER TABLE role_permission ADD PRIMARY KEY role_permission(rid,permission)"
     "create index role_permission_rid on role_permission(rid,permission)")))

(defn install []
  (create-user-table)
  (create-role-table)
  (create-user-roles-table)
  (create-role-permission-table)
  (create-object :user {
               :uid 1
               :name "admin" 
               :pass (crypt/encrypt "alice")
               :is_active true}))
  
(def updates {
  0.1  #(print "update test to ver 0.1")
  })