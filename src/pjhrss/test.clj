(ns pjhrss.test
   (:use compojure.core
         hiccup.core
         hiccup.element hiccup.form
         [hiccup.def :only [defhtml]]
        [hiccup.page :only [html5 include-js include-css]])
  (:require [pjhrss.views.layout :as layout]
            [noir.session :as session]
            [noir.util.crypt :as crypt]
            [noir.response :as response]))

(defn header []  
  [:div.navbar.navbar-fixed-top.navbar-inverse            
     [:div.navbar-inner 
      [:div.container
       [:ul.nav
        [:li (link-to "/" "Home")]
        [:li (link-to "/about" "About")]
        [:li (if-not (session/get :user) 
               (link-to "/register" "register"))]]]]])


(defn footer []
  [:footer "Copyright &copy; ..."])


(defhtml base [& content]
	(html5  
  [:head
   [:title "Welcome to test"]
   (include-css "/css/bootstrap.min.css"
                "/css/bootstrap-responsive.min.css"
                "/css/screen.css")     
   (include-js "//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"
               "/js/bootstrap.min.js")]     
  [:body content]))

(defn common [& content]
  (base
    (header)
    [:div#content content]
    (footer)))



(defn page-test []
  (common  
  	[:h1 "this is a test中文"]
  	[:h2 "this is a test中文"]
  	[:h3 "this is a test中文"]
  	[:h4 "this is a test中文"]
  	[:h5 "this is a test中文"]
   
   
   ))


(defn test-test []
  "this is a test function")

(defn init []
  (print "module test inited ...\n"))

(defn destroy []
  (print "module test destroy\n")) 

(def test-perms [
  {:path "/test" :title "test" :perm "access /test" :group "test" :menu false}
  {:path "/test2" :title "test2" :perm "access /test2" :group "test" :menu false}])

(defroutes test-routes
  (GET "/test"  []  (page-test)))