(ns pjhrss.config)
(def app-modules   
  [ 
    {:name "test" :ns 'pjhrss.test :install true }
		{:name "file" :ns 'pjhrss.models.file :install true}
		{:name "home" :ns 'pjhrss.routes.home :install false}
		{:name "menu" :ns 'pjhrss.views.menu :install true}
		{:name "node" :ns 'pjhrss.views.node :install true}
		{:name "soap" :ns 'pjhrss.views.soap :install false}
		{:name "term" :ns 'pjhrss.views.term :install true}
		{:name "users" :ns 'pjhrss.views.users :install true}
		{:name "manager" :ns 'pjhrss.views.manager :install false}
    {:name "perm" :ns 'pjhrss.views.perm :install true}   	  
    ])

(def manager-nodes-per-page 10)

(comment 
  database init and data update are set to module-install.clj
  )

