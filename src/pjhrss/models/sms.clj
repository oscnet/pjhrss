(ns pjhrss.models.sms
  (:use [clabango.parser :only [render]])     
  (:require [pjhrss.views.soap :as soap])
  (:require [pjhrss.models.excel :as excel]))
  
;https://github.com/danlarkin/clabango/blob/master/src/clabango/parser.clj

(defn row-to-map
  "head= [\"a\" \"b\"] row= [\"123\" \"456\"]
   return {\"a\" \"123\" \"b\" \"456\"}"
  [head row]
  (loop [h head r row  m {}]
    (if (or (empty? h) (empty? r))
      m
      (let [t (first h)
            v (first r)]
        (recur (rest h)  (rest r) (assoc m (keyword t) v ))))))


(defn sends 
  "群发短信,由 template 模板,模板为 {{foo}} 表示变量,m为map值的列表"
  [template,items]
  (for [item items]
    (soap/send-sms 
     (:mobie item)
     (-> template (render item)))))

(defn excel-sends
  "从excel文件发送短信,excel第一行为标题行,发送内容可以用 {{标题}}"
  [excel template]
  (let [ws (excel/workbook excel)
        rows (excel/rows (first (excel/sheets ws)))
        head (excel/values (first rows))
        rest-rows (rest rows)]
    (doseq [row rest-rows]
      (let [row (excel/values row)
            m (row-to-map head row)]
        (if-let [tel  (:手机号码 m)]
          (let [ret (soap/send-sms (clojure.string/trim tel) (-> template (render m)))]
            (println "sendsms:" tel "ret:" ret)))))))  
                    
(defn excel-send-first
  "从excel文件发送短信,只发送第一条,可用于测试"
  [excel template]
  (let [ws (excel/workbook excel)
        rows (excel/rows (first (excel/sheets ws)))
        head (excel/values (first rows))
        rest-rows (rest rows)]
      (let [row (excel/values (first rest-rows))
            m (row-to-map head row)]
        (if-let [tel  (:手机号码 m)]
          (let [ret (soap/send-sms (clojure.string/trim tel) (-> template (render m)))]
            (println "sendsms:" tel "ret:" ret))))))  
              
              
(defn -main []
  (let  [msg  (str "尊敬的退休职工{{姓名}}:请你本人携带身份证{{身份证号}}、工资卡、手机号码{{手机号码}}（也可亲属的手机号码）、"
                  "住址门牌号，在3月25号前,到县社保处(中山北路97号二楼大厅)办理享受养老金"
                  "资格认证手续。无故不参加认证，县社保机构将暂停发放退休工资。咨询电话：84182632 "
                  "[浦江县社保处] ")]  
    (excel-send-first "/Users/oscar/Downloads/退休有手机号码.xls" msg)))
  



