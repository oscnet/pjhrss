(ns pjhrss.models.file-install
  (:use pjhrss.models.system
        [taoensso.timbre :only [trace debug info warn error fatal]])
  (:require [clojure.java.jdbc :as sql]
            [pjhrss.util :as util]
            [noir.io :as io])
  (:import java.io.File))


(defn- create-file-table
  "create file table"
  []
  (sql/with-connection (db-pjhrss)
   	(sql/create-table
     :file
     [:fid "integer PRIMARY KEY AUTOINCREMENT"]
     [:uid "integer"]
     [:title	"varchar(128)"]
     [:uri	"varchar(255)"]
     [:filename "varchar(255)"]
     [:description "text"])))

(defn- create-node-file-table
  "ceate node file table"
  []
  (sql/with-connection (db-pjhrss)
  	(sql/create-table
     :node_file
     [:nid "integer"]
     [:fid "integer"])
    (sql/do-commands
     "create index node_file_nid on node_file(nid)")))

(defn- create-upload-dir []
  (let [upload (str (io/resource-path) "/uploads")
        ret (.mkdir (File. upload))]
    (if-not ret 
      (error "不能建立" upload "目录")
      false)))

(defn- add-filename []
  (sql/with-connection (db-pjhrss)
  	(sql/do-commands "alter table file add column filename varchar(255)")))

(defn install []
  (create-upload-dir)
  (create-file-table)
  (create-node-file-table))
        
(def updates {
  0.1  #(print "update test to ver 0.1")
  0.2  #(add-filename)
  0.3  #(println "nothing but for test module update")
  0.4  #(create-upload-dir)
  })