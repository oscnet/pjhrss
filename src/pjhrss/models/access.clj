(ns pjhrss.models.access
  (:use compojure.core
        pjhrss.util) 
  (:require [noir.session :as session]
            [clojure.java.jdbc :as sql]
            [pjhrss.views.perm :as perm]
            [pjhrss.util :as util]))

(defn path_permission?
  "test user has permission on path"
  ([path]
  	(let [uid (:uid (session/get :user))]
 			(path_permission? path uid))) 
  ([path uid]
    (condp = uid
      nil false
 			1		true
			(let [perms (modules_perms)]
 				(boolean 
         	(some #(and 
             			(= path (:path %))
             			(perm/user_permission? uid (:perm %))) perms))))))
  		   
(defn page-access [method url params]
  ;(println method url)
  (path_permission? url))