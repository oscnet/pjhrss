(ns pjhrss.models.html
  (:use hiccup.element
        hiccup.form
        hiccup.def
        hiccup.core)
  (:require [noir.validation :as vali]
            [noir.session :as session]))


(defmacro my-render [vm-file & other]
  (concat (list 'render (str "templates/" vm-file))
          other))

(defmacro page
  "set page by use index.vm"
  [title & items]
	(list 'layout/common title 
  	(list 'my-render 
        "index.vm" 
        :user `(:name (session/get :user))
        :manager `(~'path_permission? "/manager")  
        :main (concat (list 'html) items))))

(defn- set-form-info [field help]
  (clojure.string/join "," (conj (vali/get-errors field) help)))

(defn form-field
  [name title help & field]
  	[:div {:class  (if (vali/errors? name) "control-group error" "control-group")}
       (label {:class "control-label"} name title)
       [:div.controls
        field
        [:span.help-inline (set-form-info name help)]]])

(defn form-text-field
  ([& [name title value help placeholder]]
  (form-field name title help (text-field {:placeholder placeholder} name value)))) 

(defn form-password-field [name title value help]
  (form-field name title help (password-field name value)))

(defn form-submit-button
  ([title]
   (form-submit-button nil title))
  ([name title]
   [:div.control-group 
    [:div.controls
      (submit-button {:class "btn btn-primary" :name name} title)]]))

(defn form-buttons [& buttons]
   [:div.control-group 
    [:div.controls
      buttons]])

(defelem form [post icon title & form-fields]
  (form-to {:class "form-horizontal"} [:post post]
    [:fieldset
     [:legend [:i {:class icon}] "&nbsp" title]
 			form-fields]))

(defn breakcrumb [parent-links title]
	[:ul.breadcrumb  
   [:li (link-to "/" "首页") [:span.divider ">"]]
   	(for [link parent-links :while (not (empty? link))]
   		[:li (link-to (:path link) (:title link)) [:span.divider ">"]])
      [:li.active title]])

(defn breakcrumb-manager [parent-links title]
	[:ul.breadcrumb  
   [:li (link-to "/mindex" "管理") [:span.divider "/"]]
   	(for [link parent-links :while (not (empty? link))]
   		[:li (link-to (:path link) (:title link)) [:span.divider "/"]])
      [:li.active title]])


  
(defn description 
  "(description \"dl-horizontal\" '(\"man\" \"a male people\"))  " 
  [class items]
  [:dl (if-not (empty? class) {:class class}) 
  	(loop [[k v & r] items]
   		[:dt k]
     	[:dd v]
    	(when r (recur r)))])

(defn page-header [title & small]
  [:div.page-header
   [:h4 title 
    (if-not (empty? small)
      [:small (reduce str "" small)])]])

(defn do-script
  "do javascript script"
  [script]
  (javascript-tag script))

(defn dialog [id title body footer]
  [:div.modal.hide.fade {:id id}
   [:div.modal-header [:h3 title]]
   [:div.modal-body body]
   [:div.modal-footer footer]])
    
(defn button [title class & js-cmd]
  [:button 
   		{:class class 
    	:onclick  (clojure.string/join ";" js-cmd) }
    title])

(defn message-box
  "define the messagebox
	(message-box mybox 提示 此操作有危险,是否继续? 
		  (button 确认 btn form.submit)
  		(button 放弃 btn btn-primary (hide-dialog mybox)))
  "
  [id title message & buttons]
  (dialog id title message buttons))

(defn show-dialog [id]
	(str "$('#" id "').modal('show')"))

(defn hide-dialog [id]
	(str "$('#" id "').modal('hide')"))

;noir.session flash-put get not worked

(defn set-form-message [& msg]
  (vali/set-error :form-msg msg))

(defn show-form-message []
	(let [msg (vali/get-errors :form-msg)]
    	(if (empty? msg)
        [:div]
      	[:div.alert.fade.in
       		[:button.close {:type "button" :data-dismiss "alert"} "×"]
       		(clojure.string/join "," (first msg))])))

(defn box [title width & contents]
  [:div {:class (str "span" width)}
    [:div.box
     [:h4 title]
     [:div.boxc contents]]])

(defn row [& contents]
  [:div.row.box-row contents])

(defn left-box [title & contents]
  [:div 
    [:div.box
     [:h4 title]
     [:div.boxc contents]]])