(ns pjhrss.models.install
  (:use pjhrss.models.system)
  (:require [clojure.java.jdbc :as sql]
            [clojure.test :as test]
            [noir.util.crypt :as crypt]
            [pjhrss.config :as config]
            [pjhrss.util :as util]))


(defn- module-init-data [module-ns name f]
	(do
  	(let [ver (module-version name )]
    	(if (and (or (nil? ver) (zero? ver)) f)
      	(do 
          (f)
        	(let [updates (util/get-module-symbol module-ns "updates" "-install")]
          	(if (not (empty? updates))
            	(set-variable (str name "-ver") (apply max (keys updates)))
              (set-variable (str name "-ver") 0.0001))))))))


(defn- module-do-install
  "run module_instal/install"
  [module]
	(do
  	(let [ver (module-version (:name module))
          ns  (str (:ns module) "-install")
          name (:name module)
          fn 	(util/get-module-symbol ns "install")]
      (println "install module " name " ver:" ver "fn=" fn)
    	(if (and (or (nil? ver) (zero? ver)) fn)
      	(do 
          (fn)
          (println "module " name "installed")
        	(let [updates (util/get-module-symbol ns "updates")]
          	(if (not (empty? updates))
            	(set-variable (str name "-ver") (apply max (keys updates)))
              (set-variable (str name "-ver") 0.0001))))))))


(defn modules_install
  "run modules module_install/install function which has config :install true"
  []
  (doseq [module (filter #(boolean (:install %)) config/app-modules)]
     (module-do-install module)))

  
(defn- module-do-update [module]
	(let [ns  (str (:ns module) "-install")
        name (:name module)
        updates (util/get-module-symbol ns "updates")
        cur-version (module-version (:name module))]
    (doseq [ [k f] updates :when (> k cur-version)]
    	(f) 
      (println "module " name "updated to ver:" k)  
      (set-variable (str name "-ver") (str k)))))
      
(defn modules_update []
   (doseq [module (filter #(boolean (:install %)) config/app-modules)]
     (module-do-update module)))
  
                             
