(ns pjhrss.models.pager
   (:use pjhrss.models.system
         hiccup.util)
  (:require [clojure.java.jdbc :as sql]
            [hiccup.element :as element]))

(defmacro records [& count-query-and-params]
  `(sql/with-connection (~'db-pjhrss)
   	(sql/with-query-results ~'res
       [~@count-query-and-params]
       (first (vals (first ~'res))))))

  
(defn pages [total item-per-page]
	(let [pages (quot total item-per-page)]
    (if (> (mod total item-per-page) 0)
      (inc pages)
      pages)))

(defmacro query-page [page item-per-page  query & params]
  `(let [ first-row# (* (dec ~page) ~item-per-page)]
 		(sql/with-connection (~'db-pjhrss)
   		(sql/with-query-results ~'res
       [(str ~query " limit ?,?") ~@params first-row# ~item-per-page]
       (doall ~'res)))))


(defn- pages-show [cur total]
 {  
  :first (if (< (dec cur) 1) nil 1)
  :backward  (if (> (dec cur) 1) (dec cur) nil)
  :cur  cur
  :forward (if (< (inc cur) total) (inc cur) nil)
  :last (if (< cur total) total nil)}) 
   
  
(defn pager
  "pager (2 10 '/nodes' {:tid 1})"
  [cur total path & [ param ]]
  (let [ps (pages-show cur total)
        param (if (nil? param) {} param)]
    ;(println ps)
   [:div.pagination
    [:ul   
  		(for [k [:first :backward :cur :forward :last]]
        (if-let [page ((keyword k) ps)]
          [:li {:class (if (= cur page) "active")} 
           (element/link-to 
              (url path (assoc param :page page))
              (condp = k
                :backward [:i.icon-step-backward]
                :forward  [:i.icon-step-forward]
                page))]))]]))