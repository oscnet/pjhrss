(ns pjhrss.models.system
  (:use [noir.request :only [*request*]]
        hiccup.element
        hiccup.form
        hiccup.def
        hiccup.core
   			[noir.response :only [redirect]])
  (:require [clojure.java.jdbc :as sql]
            [noir.util.crypt :as crypt]
            [noir.validation :as vali]
            [noir.session :as session]
            [ring.velocity.core :only [render]]
            [noir.io :as io])
  (:import java.sql.DriverManager 
           java.io.File))


(defn db-path []
  (str (io/resource-path) ".." File/separator "pjhrss.sq3"))
  
(defn db [] 
  {:classname   "org.sqlite.JDBC",
   :subprotocol "sqlite",
   :subname     (db-path)})

(def db-pjhrss (memoize db))

(defn- data-initialized?
  "checks to see if the database schema is present"
  []
  (.exists (new File (db-path))))


(defn set-variable [name value]
 	(sql/with-connection (db-pjhrss)
  	(sql/update-or-insert-values :variable 
      ["name=?" name] 
      {:name name :value value})))

(defn get-variable [name] 
 	(sql/with-connection (db-pjhrss)
  	(sql/with-query-results res
    	["select value from variable where name=?" name]
      (:value (first res)))))

(defn delete-variable [name]
  (sql/with-connection (db-pjhrss)
  	(sql/delete-rows :variable ["name=?" name])))


(def get-variable-with-cache (memoize get-variable))


(defn create-variable-table 
  "create variable table"
  []
  (sql/with-connection (db-pjhrss)
  	(sql/create-table
     :variable
     [:name "varchar(128) PRIMARY KEY"]
     [:value "blob not null"])))

(defn module-version [module-name]
  (let [v (get-variable (str module-name "-ver"))]
    (if (nil? v) 0 (Double. v))))

(defn row-id
  "取 insert row 后返回的 rowid "
  [insert-ret]
  ((keyword "last_insert_rowid()") insert-ret))

(defn create-object [table obj]
  (sql/with-connection (db-pjhrss)
  	(row-id (sql/insert-record table obj))))

(defn system-init-data []
  (if (not (data-initialized?))
   (do
  	(create-variable-table)
  	(doseq [[k v] {"version" 	"1.0"
          			 "author"	 	"于晓东 oscnet@163.com"
          			 "siteName" "浦江县人力资源和社会保障网"
          			}]
    	(set-variable k v))
  	(println "system installed .."))))

