(ns pjhrss.models.file
  (:use noir.io compojure.core
        pjhrss.models.system
        pjhrss.util
        hiccup.core
        [hiccup.page :only [include-js include-css]]
        hiccup.element hiccup.form)
  (:require [noir.session :as session]
            [cheshire.core :as json]
            [noir.response :as response]
   					[pjhrss.views.layout :as layout]
            [clojure.java.jdbc :as sql]
            [noir.util.crypt :as crypt]
            [clojure.java.io :as jio]
            [noir.io :as io])
  (:import java.sql.DriverManager 
           java.io.File))

(defn db-create-file 
  "create file "
  [{:keys [uid uri title description] :as file}]
  (sql/with-connection (db-pjhrss)
  	(row-id (sql/insert-record :file file))))

(defn db-delete-file [fid]
  (sql/with-connection (db-pjhrss)
    (sql/delete-rows :node_file
      ["fid=?" fid])              
  	(sql/delete-rows :file
    	["fid=?" fid])))

(defn db-read-file [fid]
  (sql/with-connection (db-pjhrss)
  	(sql/with-query-results res
    	["select * from file WHERE fid=?" fid]
      (first res))))

(defn delete-file
  "delete file use fid alse delete the real file and node_file file table"
  [fid]  
  (let [f (db-read-file fid)
           file (str (io/resource-path) (:uri f))]
  	(jio/delete-file file true))
    (db-delete-file fid))

(defn set-node-file [nid fid] 
 	(sql/with-connection (db-pjhrss)
  	(sql/update-or-insert-values
     :node_file ["nid=? and fid=?" nid fid] {:nid nid :fid fid})))

(defn set-node-files [nid fids] 
 	(sql/with-connection (db-pjhrss)
    (doseq [fid fids]
      (sql/delete-rows :node_file
      	["nid=? and fid=?" 0 fid])
  		(sql/update-or-insert-values
     		:node_file ["nid=? and fid=?" nid fid] {:nid nid :fid fid}))))

(defn get-node-files [nid] 
 	(sql/with-connection (db-pjhrss)
  	(sql/with-query-results res
    	["select a.* from file a ,node_file b where a.fid=b.fid and b.nid=? order by a.fid" nid]
      (doall res))))

(defn delete-node-files
  "delete files that node use"
  [nid]
  (let [files (get-node-files nid)]
    (doseq [file files]
      (delete-file (:fid file)))))


(defn create-file 
  [file title description]
  (let [ uid (:uid (session/get :user))
         dir (str "/uploads/" uid "/")
         filename (:filename file)
         uri (str dir filename)
         ntitle (if (empty? title) (:filename file) title)]
 	 (.mkdirs (File. (str (io/resource-path) dir)))
   (upload-file dir file) 
   (db-create-file {:uid uid :uri uri :filename filename :title ntitle :description description})))
  
(defn own-files-not-added
  "get files upload by the current user but are not add to a node"
  [nid]
  (filter 
   #(= (:uid %) (:uid (session/get :user)))
  	(get-node-files (if (nil? nid) 0 nid))))

(defn- file-form* [nid files]
 (form-to 
    {:enctype "multipart/form-data" :id "file-form"} [:post "/add/file"]
  	[:table#file_form_table.table.table-striped.table-bordered.table-hover
    	[:thead
      	(into [:tr]
        	(for [title '("文件名" "标题" "说明" "删除")]
          	[:th title]))]
      
     	(for [{:keys [fid uri filename title description]} files]
      		[:tr
        		[:td (link-to uri (h filename))]
          	[:td (h title)]
         		[:td (h description)]
          	[:td (check-box "fid[]" false fid)]])
       
     [:tr
      [:td {:colspan 3} "&nbsp"]
      [:td (submit-button {:name "op"} "删除")]]
     [:tr
      [:td (file-upload :file_file)]
      [:td (text-field "file_title")]
      [:td (text-field "file_description")]
      [:td (hidden-field "nid" (if (nil? nid) 0 nid))
       	(submit-button {:name "op"} "上传")]]]))

(defn- file-form2* [nid]
  (let [ files (into (get-node-files nid) (own-files-not-added nid))
            fids  (map #(:fid %) files)
    		 		jfids (json/generate-string fids)]
        [:div 
         (file-form* nid files)
         (javascript-tag (str "var fileFids = " jfids ";")) 
    		 (javascript-tag "$('#file-form').submit(fileSubmit);")]))


(defn file-upload-form 
  "show the files attached by node ,and a form to upload ,delete files used by node form"
  [& [nid]]
  ;(println files)
  [:div
   [:h5 "附件:"] 
    (file-form2* nid)])
                             
(defn- delete-files [fids]
  (if (= (class fids) String)
    (delete-files [fids])
  	(do  
  		;(println "fids:" fids (class fids))
  		(doseq [fid fids]
    		(delete-file fid))
  			;(print fids);
  		(response/json {:op "del" :fids fids}))))
    
(defn file-upload-submit [op fid nid file title description]
  (println "op:" op )
  (if (= op "删除")
    (delete-files fid)    
  	(if-not (nil? (:size file))
  		(let [fid (create-file file title description)]
      	(do 
        	(set-node-file nid fid)
        	(let [f (db-read-file fid)]
        		(response/json {:op "add" :fid {
          		:uri (h (:uri f))              
          		:filename (h (:filename f))
        			:fid fid 
          		:title (h (:title f))
          		:description (h (:description description))}})))))))  
  
(defn page-upload
  "a test page for file upload"
  []
  (layout/common "文件上传" '()
    [:h2 "Upload a file"]
    (form-to {:enctype "multipart/form-data"}
             [:post "/upload"]            
             (file-upload :file)
             (label "title" "title")
             (text-field "title" )
          	 (label "description" "description")
             (text-field "desctiption")
          	 (submit-button "upload"))))
              
(defn node-files-show
  "show files on a node"
  [nid]              
  (if-let [files (get-node-files nid)]
    [:div [:i.icon-file] "附件:"
     [:ul.filelist
    	(for [{:keys [fid uri title]} files]
      	(if-not (nil? fid)
     			[:li (link-to uri [:i.icon-download-alt] (h title))]))]]))
  
(defn handle-upload 
  [file title description]
  (let [fid (create-file file title description)
        f (db-read-file fid)]    
   (response/redirect (:uri f))))

  
          
(defroutes file-routes
  (restricted GET  "/upload" [] (page-upload))
  ;(restricted POST "/delete/file/:fid" [fid] (delete-file fid))
  (restricted POST "/add/file" [op fid nid file_file file_title file_description] 
              (file-upload-submit op fid nid file_file file_title file_description)) 
  (restricted POST "/upload" [file title description] (handle-upload file title description)))
