(ns pjhrss.models.conver
  (:use pjhrss.models.system)
  (:require [clojure.java.jdbc :as sql]
            [clojure.java.io :as io]
            [noir.io :as nio]
            [pjhrss.models.fetch :as fetch]
            [pjhrss.views.node :as node]
            [pjhrss.models.file :as file]
            [pjhrss.views.term :as term])
  (:import java.sql.DriverManager
           java.io.File))
;"provider=sqloledb;initial catalog=pjhrss; data source='222.186.9.37';user id='pjhrss';password='pjhr84183226'"
(defn sql-db [] 
  {:subprotocol "jtds:sqlserver"
   :subname  "//222.186.9.37:1433/pjhrss"
   :user  "pjhrss"  
   :password  "pjhr84183226"})


(def db-sql (memoize sql-db))


(defn- get-root-term []
  (sql/with-connection (db-sql)
    (sql/with-query-results res
    	["select * from EC_type order by e_typeid"]
      (doall res))))

(defn- get-bigclass [^Integer typeid]
  (sql/with-connection (db-sql)
    (sql/with-query-results res
    	["select * from EC_BigClass where e_typeid=? order by e_bigclassid" typeid]
      (doall res))))

(defn- get-smallclass [^Integer bigid]
  (sql/with-connection (db-sql)
    (sql/with-query-results res
    	["select * from EC_smallclass where E_BigClassID=? order by e_smallclassid" bigid]
      (doall res))))

(defn- insert-conver-term [bigid smallid tid]
  (sql/with-connection (db-sql)
  	(sql/update-or-insert-values 
     :conver_term
     ["bigid=? and smallid=?" bigid smallid] 
     {:bigid bigid :smallid smallid :tid tid})))


(defn add-root-term
  "convert ec_type to term"
  []
  (doseq [t (get-root-term)]
    (println (:e_typename t))
    (if-not (term/exist? (:e_typename t))
      (do 
        (println (:e_typename t))
        (term/db-create-term {:name (:e_typename t)
                       :parent 0
                       :weight (:e_typeorder t)})
        ))))

(defn get-termid [^Integer parent name]
  (sql/with-connection (db-pjhrss)
  	(sql/with-query-results res
    	["select tid from term where name=? and parent=?" name parent]
      (:tid (first res)))))

(defn add-bigclass []
  (doseq [t (get-root-term)]
    (let [bigs (get-bigclass (:e_typeid t))
          tid (get-termid 0 (:e_typename t))]
      (doseq [big bigs]
        (println (:e_typename t) ":" )
        (if-not (term/exist? (:e_bigclassname big) tid)
          (let [tid2 
                (term/db-create-term 
                    {:name (:e_bigclassname big)
                     :parent tid
                     :weight (:E_bigclassorder big)})]
            (insert-conver-term (:e_bigclassid big) 0 tid2)))))))

(defn add-smallclass []
  (doseq [t (get-root-term)]
    (let [bigs (get-bigclass (:e_typeid t))
          tid (get-termid 0 (:e_typename t))]
      (doseq [big bigs]
        (let [smalls (get-smallclass (:e_bigclassid big))
              tid2 (get-termid tid (:e_bigclassname big))]
          (doseq [small smalls]
            (if-not (term/exist? (:e_smallclassname small) tid2)
              (let [tid3 
                (term/db-create-term 
                   {:name (:e_smallclassname small)
                     :parent tid2
                     :weight (:smallclassorder small)})]
                  (insert-conver-term 
                   (:e_bigclassid big) 
                   (:e_smallclassid small)
                   tid3)))))))))
    

(defn- create-conver-term-table
  "create conver table"
  []
  (sql/with-connection (db-sql)
  	(sql/create-table
     :conver_term
     [:bigid "integer"]
     [:smallid "integer"]
     [:tid	"integer"])
  	(sql/do-commands
     "create index term_sid on conver_term(smallid)")))

(defn- create-conver-node-table
  "create conver table"
  []
  (sql/with-connection (db-sql)
  	(sql/create-table
     :conver_node
     [:newsid "integer"]
     [:nid "integer"])
   (sql/do-commands
     "create index node_sid on conver_node(newsid)")))

(defn- create-conver-file-table
  "create conver table"
  []
  (sql/with-connection (db-sql)
  	(sql/create-table
     :conver_file
     [:attachid "integer"]
     [:fid "integer"])
   (sql/do-commands
     "create index file_sid on conver_file(attachid)")))

(defn- clear-conver []
  (sql/with-connection (db-sql)
    (sql/do-commands
      "delete from conver_term"
      "delete from conver_node"
      "delete from conver_file")))
(defn- clear-term []
  (sql/with-connection (db-pjhrss)
    (sql/do-commands
     "delete from term")))

(defn- clear-conver-node []
  (sql/with-connection (db-sql)
    (sql/do-commands
      "delete from conver_node")))

(defn get-max-newsid []
  (sql/with-connection (db-sql)
    (sql/with-query-results res
     ["select max(newsid) maxid from conver_node"]
     (:maxid (first res)))))

(defn set-conver-node [newsid nid]
  (sql/with-connection (db-sql)
    (sql/update-or-insert-values
      :conver_node
      ["newsid=? and nid=?" newsid nid]
      {:newsid newsid :nid nid})))  



(defn get-max-attachid []
  (sql/with-connection (db-sql)
    (sql/with-query-results res
     ["select max(attachid) maxid from conver_file"]
     (:maxid (first res)))))

(defn set-conver-file [attachid fid]
  (sql/with-connection (db-sql)
    (sql/update-or-insert-values
      :conver_file
      ["attachid=? and fid=?" attachid fid]
      {:attachid attachid :fid fid})))  

  
(defn read-news
  "get news which not conved"
  ([] (read-news 50 (get-max-newsid)))
  ([ret-rows] (read-news 50 (get-max-newsid)))
  ([ret-rows min-newsid]
  (sql/with-connection (db-sql)
    (sql/with-query-results res
      ["select top 100 * from ec_news where newsid>? order by newsid"  min-newsid]
      (doall res)))))

(defn get-tid [bigid smallid]
  (sql/with-connection (db-sql)
    (sql/with-query-results res
    ["select tid from conver_term where bigid=? and smallid=?" 
     bigid 
     (if (nil? smallid) 0 smallid)]
    (:tid (first res)))))

(defn text-to-string [txt]
  (with-open [rdr (java.io.BufferedReader. (.getCharacterStream txt))]
    (apply str (line-seq rdr))))

(defn- add-news []
  (set-conver-node 0 0) ; for sql max(ndwsid) work ,otherwise if not record will return null
  (doseq [new (read-news)]
    (let [nid  (node/db-create-node 
                {:title (:title new)
                 :body (text-to-string (:content new))
                 :modtime (:updatetime new)
                 :uid 1})
          tid (get-tid (:e_bigclassid new) (:e_smallclassid new))]
      (set-conver-node (:newsid new) nid)
      (term/set-node-term nid tid)
      (println "convet:" (:newsid new) "\t" tid "\t" (:e_bigclassid new)  "-" (:e_smallclassid new) "\t" (:title new))
      )))

(defn- clear-node []
  (sql/with-connection (db-pjhrss)
    (sql/do-commands
     "delete from node"
     "delete from node_term"
     )))

(defn- clear-file []
  (sql/with-connection (db-pjhrss)
    (sql/do-commands
     "delete from file"
     "delete from node_file"
     ))
  (sql/with-connection (db-sql)
    (sql/do-commands
     "delete from conver_file"
     )))


(defn get-attachs []
  (sql/with-connection (db-sql)
    (sql/with-query-results res
     ["select top 50 * from ec_attach where attachid>? and filename is not null order by attachid" (get-max-attachid)]
     (doall res))))

(defn- newsid-to-nid [newsid]
  (sql/with-connection (db-sql)
    (sql/with-query-results res
      ["select nid from conver_node where newsid=?" newsid]
      (:nid (first res)))))

(defn- add-attach []
  (set-conver-file 0 0) ; for sql max(ndwsid) work ,otherwise if not record will return null
  (doseq [attach (get-attachs)]
    (if-not (nil? (:filename attach))
      (let [filename (:filename attach)
            src (str "http://www.pjhrss.gov.cn/uploadfile/" filename)
            dst (str (nio/resource-path) File/separator "uploadfile" File/separator filename)
            nid (newsid-to-nid (:newsid attach))
            attachid (:attachid attach)]
          (println attachid filename dst)
          (.mkdirs (.getParentFile (File. dst)))
          (fetch/fetch-data src dst)
          (let [fid 
                (file/db-create-file 
                 {:uid 1 
                  :uri (str File/separator "uploadfile" File/separator filename)
                  :filename (.getName (File. dst))})]
            (file/set-node-file nid fid)
            (set-conver-file attachid fid))))))   
      

(defn conver-node [nodes]
  (doseq [node nodes]
    (let [body (:body node)
          new-body (clojure.string/replace body #"uploadfile/" "/uploadfile/" )]
      (println "conver " (:nid node))
      ;(sql/with-connection (db-pjhrss)
        (sql/update-values :node 
          ["nid=?" (:nid node)]
          {:body new-body}))))

(defn conver-nodes []
  (sql/with-connection (db-pjhrss)
    (sql/with-query-results res
     ["select * from node order by nid"]
     (conver-node res))))
  
                           
        
(defn -main 
  "the main function to conver sql server to new database 
  use lein run -m pjhrss.models.conver"
  []
  (do
    (println "ok, I am running!")
    
    ;----------- term -------------------------------------
    ;(create-conver-term-table)
    ;(create-conver-node-table)
    ;(create-conver-file-table)
    ;(clear-conver)
    ;(clear-term)
    ;(add-root-term)
    ;(add-bigclass)
    ;(add-smallclass)
    
    ;--------- for news to node ---------------------------
    ;(clear-conver-node)
    ;(clear-node)
    
    ;(add-news)
    ;(clear-file)
    ;(add-attach)
    (conver-nodes)
    
    ))


(comment 
  
  总栏 EC_type (E_typeid E_typename E_typeorder) 
  EC_BigClass (E_BigClassID  E_BigClassName E_typeid E_bigclassorder )  
  EC_smallclass(E_SmallClassID E_BigClassID E_typeid smallclassorder E_smallclassname )
  EC_News(NewsID Title checked=1 Author editor UpdateTime(修改时间 datetime) Content click 点击数 E_typeid E_bigclassid
                 E_smallclassid picname ) 
  附件  EC_attach (attachid newsid filename)
  )

(comment 
  SQLite ----> MySql
Download sqlite3.exe on http://www.sqlite.org
Export the SQLite database with sqlite3.exe and command parameter ".dump" 
  (Nate, ".export" doesn't exist!), an example 
sqlite3 mySQLiteDataBase .dump .quit >> myDumpSQLite
Adapt the dump to get it compatible for MySQL
  - Replace " (double-quotes) with ` (grave accent) 
  - Remove "BEGIN TRANSACTION;" "COMMIT;", and lines related to "sqlite_sequence"
  - Replace "autoincrement" with "auto_increment"
The dump is ready to get imported in a MySQL server  
  
  )