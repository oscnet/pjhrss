(ns pjhrss.handler
  (:use compojure.core
        pjhrss.models.system
        [taoensso.timbre :only [trace debug info warn error fatal]])  
  (:require 
   					[noir.util.middleware :as middleware]
            [pjhrss.models.access :as access]
            [pjhrss.models.install :as install]
            [pjhrss.util :as util]
            [compojure.route :as route]))

(util/require_modules)

(defn init
  "init will be called once when
   app is deployed as a servlet on 
   an app server such as Tomcat
   put any initialization code here"
  [] 
  (system-init-data)
  (install/modules_install)
  (install/modules_update)  
  (util/modules-run "init")
  
  (info "pjhrss started..."))
 
(defn destroy
  []
  (util/modules-run "destroy")
  (println "pjhrss destroyed"))


(defroutes app-routes  
  (route/resources "/")
  (route/not-found "Not Found"))


(def all-routes (util/modules-routes app-routes))

(defn wrap-test [handler]
  (fn [request]
    ;(println "request" request)
    (let [response (handler request)]
    ;    (println "response:" response)
       	response
       )))

(def app (->
      			 all-routes 
             (middleware/app-handler)
          	 (wrap-test)	
             (middleware/wrap-access-rules access/page-access)))
(def war-handler (middleware/war-handler app))