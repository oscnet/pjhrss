(ns pjhrss.test.handler
  (:use clojure.test
        ring.mock.request  
        pjhrss.handler))

(deftest test-app
  (testing "main route"
    (let [response (app (request :get "/"))]
      (is (= (:status response) 200))
      (is (.contains (:body response) "人力资源和社会保障")))) 
        
  (testing "not-found route"
    (let [response (app (request :get "/invalid"))]
      (is (= (:status response) 404)))))
