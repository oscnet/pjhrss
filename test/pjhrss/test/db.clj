(ns pjhrss.test.db
  (:use clojure.test
        pjhrss.util
        pjhrss.models.system))

(deftest test-variable
  (testing "test variable function "
    (let [v2 (get-variable "#test")
          v3 (get-variable "#123")]
      (is (= v2 "test"))
      (is (= v3 "中文")))))     
      
         
(defn fixture-variable [test-function]
  ; Perform setup here.
  (set-variable "#test" "test")
  (set-variable "#123" "中文")
  (test-function)
  ; Perform teardown here.
  (delete-variable "#test")
  (delete-variable "#123")
)

;(use-fixtures :each fixture-1 fixture-2 ...)

(use-fixtures :once fixture-variable)
