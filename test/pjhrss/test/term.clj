(ns pjhrss.test.term
  (:use clojure.test
        pjhrss.util
        pjhrss.views.node
        pjhrss.views.term))

(def vars {})

(deftest test-term
  (testing "create term"  
    (let [tid (:tid vars)
          term (db-read-term tid)]
      (is (> tid 0))
      (is (= (:name term "test")))))
    
  (testing "set node term"
    (let [nid (:nid vars)
          tid (:tid vars)
          _ (set-node-term nid tid)]
      (is (= tid  (first-node-term nid)))))
      
  (testing "terms Relationship"
    (let [terms (:terms vars)
          {:keys [a a1 a12 c1 c]} terms]
      (is (= (map #(:tid %) (parent-terms a12)) [a a1 a12]))
      (is (= (map #(:tid %) (parent-terms c1)) [c c1])))))    
          
(defn- create-terms []
  (let [
		a		(db-create-term {:name "a" 		:parent 0 	:weight "2"})
    b 	(db-create-term {:name "b" 		:parent 0 	:weight "1"})
    c 	(db-create-term {:name "c" 		:parent 0 	:weight 3	 })
    a1 	(db-create-term {:name "a1" 	:parent a 	:weight "1"})
    a2 	(db-create-term {:name "a2" 	:parent a 	:weight 2  })
    a11 (db-create-term {:name "a11" 	:parent a1	:weight -1 })
    a12 (db-create-term {:name "a12" 	:parent a1 	:weight 1  })
    a21 (db-create-term {:name "a21" 	:parent a2 	:weight "1"})
    c1 	(db-create-term {:name "c1" 	:parent c 	:weight "2"})]
    {:a a :b b :c c :a1 a1 :a2 a2 :a11 a11 :a12 a12 :a21 a21 :c1 c1}))
    

(defn fixture-term [test-function]
  (let [nid (db-create-node {:title "title" :body "body"})
        tid (db-create-term {:name "test" :parent 0 })
        terms (create-terms)]
    	(def vars (assoc vars :nid nid :tid tid :terms terms))   
      (test-function)
  		(db-delete-node (:nid vars))
      (db-delete-term (:tid vars))
      (doseq [[k v] terms]
        (db-delete-term v))))

(use-fixtures :once fixture-term)