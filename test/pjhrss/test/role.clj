(ns pjhrss.test.role
  (:use clojure.test
        pjhrss.util
        pjhrss.views.perm
        pjhrss.views.users
        pjhrss.models.access))


(def vars {})

(deftest test-term
  (testing "role permission"  
    (let [uid (:uid vars)
          rid (:rid vars)]
      (is (user_permission? uid "allow upload") "test if user has the permission")
      (is (complement (user_permission? uid "#allow admin#")) "test if user not has the permission")
	    (is (user_permission? 1 "#allow admin")  "admin user always has all permission")
	    (is (user_permission? 1 "#######")  "admin user always has all permission"))))
  
  
(defn fixture-role [test-function]
  (let [uid (create-user {:name "#test"})
        rid (create-role {:name "#role-1" :weight 1})]
    	(set-role-permission rid "allow upload")
    	(add-user-roles rid uid)
    	(def vars (assoc vars :uid uid :rid rid))   
      (test-function)
  		(delete-role rid)
      (delete-user uid)))

(use-fixtures :once fixture-role)