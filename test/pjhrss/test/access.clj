(ns pjhrss.test.access
  (:use clojure.test
        pjhrss.util
        pjhrss.models.access))
	
(deftest test-path-perm
  (testing "path_permission"  
    (let [perm-no (path_permission? "/manager" 345151)
          perm-yes (path_permission? "/manager" 1)]
      (is (not perm-no))
      (is perm-yes))))