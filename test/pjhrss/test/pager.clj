(ns pjhrss.test.pager
  (:use clojure.test
        pjhrss.util
        pjhrss.models.pager
        pjhrss.models.access))
	
(deftest test-pager
  (testing "pager pages"  
    (let [t1 (pages 100,10)
          t2 (pages 10,3)
          t3 (pages 33,10)]
      (is (= t1 10))
      (is (= t2 4))
      (is (= t3 4))))
  (testing "pager pager"
    (let [pages1 (pages-show 5 6)
          pages2 (pages-show 3 6)]
      (is (= pages1 '( 2 3 4 5 6)))
      (is (= pages2 '( 1 2 3 4 5 6))))))
      
      
  
  