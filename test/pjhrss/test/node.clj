(ns pjhrss.test.node
  (:use clojure.test
        pjhrss.util
        pjhrss.views.node))

(def test-nid 0)

(deftest test-node
  (testing "node"
    (let [node (db-read-node test-nid)]
      (is (= (:nid node) test-nid))
      (is (= (:title node) "title"))
      (is (= (:body node) "body")))))     
      
(defn fixture-node [test-function]
  (let [nid (db-create-node {:title "title" :body "body"})]
  	(do
  		(def test-nid nid)
      (test-function)
  		(db-delete-node nid))))

(use-fixtures :once fixture-node)
