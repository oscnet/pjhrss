(ns pjhrss.test
   (:use [jayq.core :only [$ css inner]]))

(.write js/document "<div id=\"jquery-test\">hello,clojurescript! this line is blue</div>")
(def $interface ($ :#jquery-test))
(-> $interface
     (css {:color "blue"}))