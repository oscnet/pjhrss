(ns pjhrss.node
   (:use [jayq.core :only [$ val on]]))

;ckeditor must set the body value before form submited whith jquery form plugin.
(defn fixCkeditor []
  (let [editor_data (js/CKEDITOR.instances.body.getData)]
    (val ($ :#body) editor_data)))
        
        
        
(defn file-submit []
  ($ this))
  
  
;$('#myTable tr:last').after('<tr>...</tr><tr>...</tr>');
;var obj = jQuery.parseJSON('{"name":"John"}');  
;alert( obj.name === "John" );    
  
(on ($ :#file-form) "submit" file-submit)
