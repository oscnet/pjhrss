$(function(){ 
    // fix sub nav on scroll
    var $win = $(window)
      , $nav = $('.subnav')
      , navTop = $('.subnav').length && $('.subnav').offset().top - 40
      , isFixed = 0

    processScroll()

    $win.on('scroll', processScroll)

    function processScroll() {
      var i, scrollTop = $win.scrollTop()
      if (scrollTop >= navTop && !isFixed) {
        isFixed = 1
        $nav.addClass('subnav-fixed')
      } else if (scrollTop <= navTop && isFixed) {
        isFixed = 0
        $nav.removeClass('subnav-fixed')
      }
    }
  
    $(".box").hover(
      function () {
        $(this).addClass("box-hover");
      },
      function () {
        $(this).removeClass("box-hover");
      });   
})
  
//use for manager page    
var options = { 
 		target:	'#main_area', 
    success: set_main_area} 
         
function set_main_area(){
		$('#main_area a').click(setClick);
		$('form').ajaxForm(options);
}
 
function setClick(){
		$.get(this, function(result){
			$('#main_area').html(result);
		  set_main_area();   
		});   			
		return false;        	
}


/* for file upload */
function deleteFid (fid){
 for(var j=0,jlen=fileFids.length; j<jlen; j++){
     if(fid == fileFids[j]){
       //delete fileFids[j];
       fileFids.splice(j,1);
       return;
     }
 } 
}

function deleteFileLine(fid){
  var sel="#file_form_table tr input[value="+fid +"]";
  //alert(sel);
  $(sel).parent().parent().remove();
}

function deleteLine(fids){
 for(var i=0,len=fids.length; i<len; i++){
   deleteFid(fids[i]);
   deleteFileLine(fids[i]);
	}   
}

function fileUploaded(data){
  //alert(data.op);
  if(data.op == "add"){
    var f = data.fid;
 		var newtr = "<tr><td><a href='"+f.uri+"'>"+f.filename+"</a></td>"
 		newtr = newtr + "<td>"+f.title+"</td>"
 		newtr = newtr + "<td>"+f.description+"</td>"
 		newtr = newtr + '<td><input name="fid[]" type="checkbox" value="'+f.fid+'"></td></tr>'
 		$('#file_form_table tr:eq(1)').before(newtr);
 		fileFids.push(f.fid);
  	//alert(fileFids);
    $('#file-form')[0].reset();
  }else if(data.op == "del"){
    //alert ("delete fids " + data.fids);
    deleteLine(data.fids);
  }
}

function fileSubmit(){
  var fileOptions = {
    dataType: 'json',
    success:  fileUploaded 
  };     
  $(this).ajaxSubmit(fileOptions);
  return false; 
};



  
    